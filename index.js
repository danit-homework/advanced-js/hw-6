// Теоритичне завдання:
// У Javascript асинхронність означає, що певні дії, які зазвичай виконуються послідовно, можуть виконуватися паралельно.
// Наприклад, коли ми викликаємо функцію, яка займає деякий час на обробку, замість того, щоб чекати, поки функція завершить 
// свою роботу, ми можемо продовжувати виконувати інші дії, за допомогою асинхроного коду, використовуючи проміси
// та async/await.

let btn = document.querySelector('.btn');
let locationWrap = document.querySelector('.wrap');

btn.addEventListener('click', () => toFetchOurIp());

async function toFetchOurIp() {
	try {
		let response = await fetch('https://api.ipify.org/?format=json');
		let myIp = await response.json();

		let location = await fetch(`http://ip-api.com/json/${myIp.ip}?fields=status,message,continent,country,regionName,city,district,query`)
		let myLocation = await location.json();

		renderLocation(myLocation);
	} catch (error) { console.log(error) }
}

function renderLocation(location) {

	let { continent, country, regionName, city, district } = location;

	let locationHolder = document.createElement('ul');
	locationHolder.className = "locationHolder";
	locationWrap.append(locationHolder);

	locationHolder.insertAdjacentHTML('beforeend', `
    <li><b>Твій континент:</b> ${continent}</li>
    <li><b>Твоя країна:</b> ${country}</li>
    <li><b>Твій регіон:</b> ${regionName}</li>
    <li><b>Твоє місто:</b> ${city}</li>
    <li><b>Твій район:</b> ${district}</li>
    `)
}
